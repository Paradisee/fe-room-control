import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './layouts/main-layout/main-layout.component';
import { LoginLayoutComponent } from './layouts/login-layout/login-layout.component';

const routes: Routes = [
  {
    path: 'auth',
    component: LoginLayoutComponent,
    loadChildren: () => import('./pages/auth/auth.module').then(mod => mod.AuthModule)
  }, {
    path: 'applications',
    component: MainLayoutComponent,
    loadChildren: () => import('./pages/applications/applications.module').then(mod => mod.ApplicationsModule)
  }, {
    path: 'cameras',
    component: MainLayoutComponent,
    loadChildren: () => import('./pages/cameras/cameras.module').then(mod => mod.CamerasModule)
  }, {
    path: 'weather',
    component: MainLayoutComponent,
    loadChildren: () => import('./pages/weather/weather.module').then(mod => mod.WeatherModule)
  }, {
    path: 'chuck-norris',
    component: MainLayoutComponent,
    loadChildren: () => import('./pages/chuck-norris/chuck-norris.module').then(mod => mod.ChuckNorrisModule)
  }, {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
