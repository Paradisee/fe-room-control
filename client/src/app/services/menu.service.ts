import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  isSideBarVisible: boolean = false;

  sidebarVisibilityChange: Subject<boolean> = new Subject<boolean>();

  constructor() {
    this.sidebarVisibilityChange.subscribe(value => {
      this.isSideBarVisible = value
    });
  }

  toggleSidebarVisibility() {
    this.sidebarVisibilityChange.next(!this.isSideBarVisible);
  }

}
