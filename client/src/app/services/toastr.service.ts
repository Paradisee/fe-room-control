import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastrMessageService {

  constructor(private toastrService: ToastrService) {

  }

  showSuccess(title: string, message: string) {
    this.toastrService.success(message, title, { positionClass: 'toast-bottom-center' });
  }

  showError(title: string, message: string) {
    this.toastrService.error(message, title, { positionClass: 'toast-bottom-center' });
  }

}
