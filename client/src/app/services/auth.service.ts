import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const appApi: string = environment.appApi;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {

  }

  signIn(username: string, password: string) {
    return this.http.post(appApi + 'auth/sign-in', { username, password });
  }

}
