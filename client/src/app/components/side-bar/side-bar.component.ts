import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { MenuService } from 'src/app/services/menu.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {

  isVisible: boolean = false;

  constructor(
    private router: Router,
    private menuService: MenuService) {
    this.menuService.sidebarVisibilityChange.subscribe(value => {
      this.isVisible = value;
    })
  }

  ngOnInit() {
  }

  navigateTo(path: Array<string>) {
    this.menuService.toggleSidebarVisibility();
    this.router.navigate(path);
  }

}
