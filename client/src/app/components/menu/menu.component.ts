import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/services/menu.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(
    private router: Router,
    private menuService: MenuService) { }

  ngOnInit() {
  }

  navigateTo(url: Array<string>) {
    this.router.navigate(url);
  }

  openMenu() {
    this.menuService.toggleSidebarVisibility();
  }

}
