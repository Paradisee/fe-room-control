import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.scss']
})
export class ApplicationsComponent implements OnInit {

  applications: Array<any> = [
    {
      name: 'Weather',
      description: 'Random description.',
      icon: 'ICON',
      url: ['weather']
    }, {
      name: 'Chuck Norris',
      description: 'It is a free JSON API for hand curated Chuck Norris facts.',
      icon: 'ICON',
      url: ['chuck-norris']
    }, {
      name: 'App 2',
      description: 'Random description.',
      icon: '',
      url: null
    }, {
      name: 'App 3',
      description: 'Random description.',
      icon: 'ICON',
      url: null
    }, {
      name: 'App 4',
      description: 'Random description.',
      icon: 'ICON',
      url: null
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
