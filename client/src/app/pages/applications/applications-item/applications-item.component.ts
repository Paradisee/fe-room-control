import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-applications-item',
  templateUrl: './applications-item.component.html',
  styleUrls: ['./applications-item.component.scss']
})
export class ApplicationsItemComponent implements OnInit {

  @Input() application: any;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateTo(path: Array<string>) {
    if (path === null) {
      return;
    }

    this.router.navigate(path);
  }

}
