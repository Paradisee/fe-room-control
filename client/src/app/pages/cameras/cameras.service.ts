import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const appApi: string = environment.appApi;

@Injectable({
  providedIn: 'root'
})
export class CamerasService {

  constructor(private http: HttpClient) {
  }

  addCamera(item: any) {
      return this.http.post(appApi+'cameras', item);
  }

}
