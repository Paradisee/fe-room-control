import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

import { CamerasRoutingModule } from './cameras-routing.module';
import { CamerasComponent } from './cameras.component';
import { CameraItemComponent } from './camera-item/camera-item.component';

@NgModule({
  declarations: [
    CamerasComponent,
    CameraItemComponent,
  ],
  imports: [
    CommonModule,
    CamerasRoutingModule,
    ReactiveFormsModule,
    ModalModule.forRoot()
  ]
})
export class CamerasModule { }
