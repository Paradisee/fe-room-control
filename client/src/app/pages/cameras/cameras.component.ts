import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { CamerasService } from './cameras.service';

@Component({
  selector: 'app-cameras',
  templateUrl: './cameras.component.html',
  styleUrls: ['./cameras.component.scss']
})
export class CamerasComponent implements OnInit {

  modalRef: BsModalRef;

  addCameraForm = new FormGroup({
    name: new FormControl('', Validators.required),
    floor: new FormControl(null),
    url: new FormControl('', Validators.required),
  });

  cameras: Array<any> = [
    {
      name: 'Camera 1',
      floor: 1,
      url: '/assets/images/cameras/placeholder.png'
    }
  ];

  constructor(
    private modalService: BsModalService,
    private camerasService: CamerasService) { }

  ngOnInit() {
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  addCamera() {
    if (!this.addCameraForm.valid) {
      return;
    }

    const name: string = this.addCameraForm.value.name;
    const floor: string = this.addCameraForm.value.floor;
    const url: string = this.addCameraForm.value.url;

    const data: any = { name, floor, url };

    this.camerasService.addCamera(data).subscribe(
      (result: any) => {
        console.log(result);
      }
    );
  }

}
