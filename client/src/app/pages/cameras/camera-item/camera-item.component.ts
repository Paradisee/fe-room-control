import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-camera-item',
  templateUrl: './camera-item.component.html',
  styleUrls: ['./camera-item.component.scss']
})
export class CameraItemComponent implements OnInit {

  @Input() camera: any;

  constructor() { }

  ngOnInit() {
  }

}
