import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const weatherUrl: string = environment.weatherUrl;
const weatherApiKey: string = environment.weatherApiKey;

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }

  getWeatherByCityName(name: string) {
    return this.http.get(weatherUrl+'weather?q='+name+'&APPID='+weatherApiKey+'&units=metric');
  }

}
