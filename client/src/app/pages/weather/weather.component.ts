import { Component, OnInit } from '@angular/core';

import { WeatherService } from './weather.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

  data: any = {
    main: {
      humidity: null,
      temp_max: null,
      temp_min: null,
      temp: null
    },
    clouds: {
      all: null
    },
    wind: {
      speed: null
    },
    sys: {
      country: null
    },
    name: null
  };

  weatherIconPath = '';
  weatherIconAlt = '';

  constructor(private weatherService: WeatherService) { }

  ngOnInit() {
    this.getWeatherByCityName('name');
  }

  getWeatherByCityName(name: string) {
    this.weatherService.getWeatherByCityName('mentana').subscribe(
      (result: any) => {
        console.log(result);
        result.main.temp = Math.round(result.main.temp);
        result.main.temp_max = Math.round(result.main.temp_max);
        result.main.temp_min = Math.round(result.main.temp_min);

        this.data = result;

        this.setWeatherIcon(result.weather[0]);
      }, error => console.log(error)
    );
  }

  setWeatherIcon(type: any) {
    const baseUrl = 'assets/images/weather/';

    const description: string = type.description;

    switch (type.main) {
      case 'Clouds': {
        const icon: string = (this.isDayTime) ? '003-cloudy-day.svg' : '003-cloudy-night.svg';
        this.weatherIconPath = baseUrl + icon;
        break;
      }
      case 'Clear': {
        const icon: string = (this.isDayTime) ? '002-sun.svg' : '002-night.svg';
        this.weatherIconPath = baseUrl + icon;
        break;
      }
      case 'Rain': {
        if (description === 'light rain') {
          const icon: string = (this.isDayTime) ? '001-rainy-day.svg' : '001-rainy-night.svg';
          this.weatherIconPath = baseUrl + icon;
        }

        break;
      }
      default: {
        this.weatherIconPath = '';
        break;
      }
    }
  }

  isDayTime(): boolean {
    const hours = new Date().getHours();

    if (hours > 6 && hours < 20) {
      return true;
    }

    return false;
  }

}
