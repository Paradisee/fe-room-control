import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AuthService } from 'src/app/services/auth.service';
import { ToastrMessageService } from 'src/app/services/toastr.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  signInForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(
    private router: Router,
    private authService: AuthService,
    private toastrMessageService: ToastrMessageService) { }

  ngOnInit() {
  }

  signIn() {
    if (!this.signInForm.valid) {
      return;
    }

    const username: string = this.signInForm.value.username;
    const password: string = this.signInForm.value.password;

    this.authService.signIn(username, password).subscribe(
      (result: any) => {
        console.log(result);
        this.router.navigate(['weather'])
      }, error => {
        console.log(error)
        if (error.status === 401) {
          this.toastrMessageService.showError('', 'Invalid credentials.');
          return
        }

        this.toastrMessageService.showError('', 'The server is unavailable, please try later.');
      }
    );
  }

}
