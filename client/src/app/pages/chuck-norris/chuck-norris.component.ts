import { Component, OnInit } from '@angular/core';

import { ChuckNorrisService } from './chuck-norris.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-chuck-norris',
  templateUrl: './chuck-norris.component.html',
  styleUrls: ['./chuck-norris.component.scss']
})
export class ChuckNorrisComponent implements OnInit {

  categories: Array<string> = [];
  quote: string = '';

  constructor(
    private toastrService: ToastrService,
    private chuckNorrisService: ChuckNorrisService) { }

  ngOnInit() {
    this.getCategories();
  }

  getRandomQuote() {
    this.chuckNorrisService.getRandomQuote().subscribe(
      (result: any) => {
        this.quote = result.value;
      }
    ), error => {
      this.toastrService.error('Error');
    }
  }

  getCategories() {
    this.chuckNorrisService.getCategories().subscribe(
      (result: Array<string>) => {
        this.categories = result;
      }
    );
  }

  getCategoryRandomQuote(query: string) {
    this.chuckNorrisService.getCategoryRandomQuote(query).subscribe(
      (result: any) => {
        this.quote = result.value;
      }
    )
  }

}
