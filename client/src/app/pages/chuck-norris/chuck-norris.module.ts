import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChuckNorrisRoutingModule } from './chuck-norris-routing.module';
import { ChuckNorrisComponent } from './chuck-norris.component';

@NgModule({
  declarations: [
    ChuckNorrisComponent,
  ],
  imports: [
    CommonModule,
    ChuckNorrisRoutingModule
  ]
})
export class ChuckNorrisModule { }
