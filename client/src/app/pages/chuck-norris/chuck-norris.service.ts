import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const chuckNorrisUrl: string = environment.chuckNorrisUrl;

@Injectable({
  providedIn: 'root'
})
export class ChuckNorrisService {

  constructor(private http: HttpClient) { }

  getRandomQuote() {
    return this.http.get(chuckNorrisUrl+'random');
  }

  getCategories() {
    return this.http.get(chuckNorrisUrl+'categories');
  }

  getCategoryRandomQuote(query: string) {
    return this.http.get(chuckNorrisUrl+'random?category='+query);
  }

  searchQuote(query: string) {
    return this.http.get(chuckNorrisUrl+'search?query='+query);
  }

}
